# Default chooser
default:
  @just --choose

# Generate cli docs
docs:
  typer oiext.main utils docs --name oiext --output docs/cli.md

# Format python code
fmt:
  ruff . --fix
  ruff format .

# Run all pre commit hooks
pre:
  pre-commit run -a

# Create a changelog
changelog:
  cz bump

# Ad hoc testing
ad-hoc:
 oiext tests/assets/img/test.jpg

# Run docker instance for testing
instance:
  docker run -v$(pwd):/app -it python:alpine sh

# Build the image
build:
  docker build -f docker/Dockerfile -t oiext:$(poetry version -s) .

# Run oiext container
run:
  docker run -it -v$(pwd):/app  oiext:$(poetry version -s) oiext tests/assets/img/test.jpg
