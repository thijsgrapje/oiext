# `oiext`

Osint Image EXTractor

**Usage**:

```console
$ oiext [OPTIONS] PATH [OUTPUT_FILE]
```

**Arguments**:

* `PATH`: Path to image(s): ['.jpg', '.jpeg', '.png', '.gif', '.bmp'].  [required]
* `[OUTPUT_FILE]`: Path to output: ['.txt', '.json', '.csv', '.vcf']  [default: output.txt]

**Options**:

* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.
