# Osint Image EXTractor

This project is for extracting phone numbers from a pictures for OSINT purposes.

## Dependencies
Main:
- [tesseract ocr](https://tesseract-ocr.github.io/tessdoc/Installation.html)
- [pytesseract](https://github.com/h/pytesseract)
- [python-phonenumbers](https://github.com/daviddrysdale/python-phonenumbers)
- [typer](https://github.com/tiangolo/typer)

Dev:
- [poetry](https://github.com/python-poetry/poetry)
- [just](https://github.com/casey/just)

### On fedora
``` bash
sudo dnf install R-tesseract
```
## Installation
Getting started:
``` bash
# Clone repo
git clone https://gitlab.com/theintegrative/pic-phone-number-extractor
# Change directory
cd pic-phone-number-extractor
# Install dependencies
poetry install 
# Osint Image EXTractor help 
oiext --help
```

### Docker
Build docker image:
```
docker build -f docker/Dockerfile -t oiext:$(poetry version -s) .

```
Run with docker:
```
docker run -it -v$(pwd):/app  oiext:$(poetry version -s) oiext tests/assets/img/test.jpg

```
> [Read the docs](docs/cli.md)
