import csv
import json
from pathlib import Path
import phonenumbers
import pytesseract
import typer
from PIL import Image
import concurrent.futures

# Settings
pytesseract.pytesseract.tesseract_cmd = r"/usr/bin/tesseract"

image_extensions = [".jpg", ".jpeg", ".png", ".gif", ".bmp"]
output_extensions = [".txt", ".json", ".csv", ".vcf"]
command = "oiext"
app = typer.Typer()


def process_image(image_path: Path):
    image = Image.open(image_path)
    text = pytesseract.image_to_string(image)
    extracted = phonenumbers.PhoneNumberMatcher(text, None)
    results = []
    for match in extracted:
        results.append(
            {
                "country": phonenumbers.region_code_for_number(match.number),
                "number": phonenumbers.format_number(
                    match.number, phonenumbers.PhoneNumberFormat.E164
                ),
            }
        )
    return results


def save_to_txt(results, output_file):
    def write():
        with open(output_file, "w") as file:
            [file.write(f"{result['number']}\n") for result in results]

    return write


def save_to_json(results, output_file):
    def write():
        with open(output_file, "w") as file:
            json.dump(results, file, indent=4)

    return write


def save_to_csv(results, output_file):
    def write():
        keys = results[0].keys() if results else []
        with open(output_file, "w", newline="") as file:
            writer = csv.DictWriter(file, fieldnames=keys)
            writer.writeheader()
            writer.writerows(results)

    return write


def save_to_vcf(results, output_file):
    def write():
        with open(output_file, "w") as file:
            for contact in results:
                file.write("BEGIN:VCARD\n")
                file.write("VERSION:2.1\n")
                file.write(f"TEL;CELL:{contact['number']}\n")
                file.write("END:VCARD\n")

    return write


@app.command(help="Osint Image EXTractor")
def main(
    path: str = typer.Argument(help=f"Path to image(s): {image_extensions}."),
    output_file: str = typer.Argument(
        default="output.txt", help=f"Path to output: {output_extensions}"
    ),
):
    path = Path(path)
    output_file = Path(output_file)

    if not path.exists():
        typer.echo("Input path does not exist.")
        raise typer.Exit(code=1)

    processed = []

    if path.is_file():
        if path.suffix.lower() in image_extensions:
            processed.extend(process_image(path))
    elif path.is_dir():
        with concurrent.futures.ProcessPoolExecutor() as executor:
            futures = [
                executor.submit(process_image, file)
                for file in path.glob("**/*")
                if file.is_file() and file.suffix.lower() in image_extensions
            ]
            for future in concurrent.futures.as_completed(futures):
                processed.extend(future.result())

    format_extensions = {
        ".txt": save_to_txt(processed, output_file),
        ".json": save_to_json(processed, output_file),
        ".csv": save_to_csv(processed, output_file),
        ".vcf": save_to_vcf(processed, output_file),
    }

    format_options = format_extensions.keys()
    format = output_file.suffix.lower()

    if format not in format_options:
        typer.echo(f"Invalid output format {format}. Options {format_options}")
        raise typer.Exit(code=1)

    format_extensions[format]()
    typer.echo(f"Results saved to {output_file}.")


if __name__ == "__main__":
    typer.run(main)
